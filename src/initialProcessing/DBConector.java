/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package initialProcessing;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Marwa Gamal <http://marwagamal.com/>
 */
public class DBConector {

    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/ArQuesGen";
    private static final String DB_UNICODE = "?useUnicode=yes&characterEncoding=UTF-8";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    public void saveScriptText() {

    }

    public static void insertQues(String ques, int type, String options) throws SQLException {

        Connection dbConnection = null;
        Statement statement = null;

        String insertTableSQL = "INSERT INTO quesLst(quesTxt,QuesTypeID,options)"
                + "VALUES('" + ques + "','" + type + "','" + options + "')";

        try {
            dbConnection = getDBConnection();
            statement = (Statement) dbConnection.createStatement();

            System.out.println(insertTableSQL);

            // execute insert SQL stetement
            statement.executeUpdate(insertTableSQL);

            System.out.println("Question is inserted into quesLst table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
    }

    public static void insertIntoScriptDBTable(String scriptfile) throws SQLException {

        Connection dbConnection = null;
        Statement statement = null;

        String insertTableSQL = "INSERT INTO FullTxt(text,NoChapters) "
                + "VALUES('" + scriptfile + "',1)";

        try {
            dbConnection = getDBConnection();
            statement = (Statement) dbConnection.createStatement();

            System.out.println(insertTableSQL);

            // execute insert SQL stetement
            statement.executeUpdate(insertTableSQL);

            System.out.println("Record is inserted into DB table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }

    }

    public static void insertSentLst(String fullSent, String normalizedSent,
            String segmentedSent, String parsedTree, String perEntity, String locEntity, String orgEntity, String keywords)
            throws SQLException {

        Connection dbConnection = null;
        Statement statement = null;

        String insertTableSQL = "INSERT INTO sentenceLst(fullSent,normalizedSent,"
                + "tokenizedSent,parseSent,perEntity,locEntity,orgEntity,keywords,timeOrDate)"
                + "VALUES('" + fullSent + "','" + normalizedSent + "','"
                + segmentedSent + "','" + parsedTree + "','" + perEntity
                + "','" + locEntity + "','" + orgEntity + "','" + keywords
                + "','" + getCurrentTimeStamp() + "')";

        try {
            dbConnection = getDBConnection();
            statement = (Statement) dbConnection.createStatement();

            System.out.println(insertTableSQL);

            // execute insert SQL stetement
            statement.executeUpdate(insertTableSQL);

            System.out.println("Record is inserted into sentenceLst table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }

    }

    private static java.sql.Timestamp getCurrentTimeStamp() {

        java.util.Date today = new java.util.Date();
        return new java.sql.Timestamp(today.getTime());

    }

    public static List<String> selectSentLst() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        List<String> sentLst = new LinkedList<>();
        String selectTableSQL = "select normalizedSent, parseSent, perEntity,"
                + " locEntity, orgEntity from sentenceLst";

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            System.out.println(selectTableSQL);

            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                String per = rs.getString("perEntity");
                String loc = rs.getString("locEntity");
                String org = rs.getString("orgEntity");
                if (per.isEmpty() || per.equals("-")) {
                    per = " ";
                }
                if (loc.isEmpty() || loc.equals("-")) {
                    loc = " ";
                }
                if (org.isEmpty() || org.equals("-")) {
                    org = " ";
                }

                String sent = rs.getString("normalizedSent") + "***"
                        + rs.getString("parseSent") + "***"
                        + per + "***"
                        + loc + "***"
                        + org + "***";
                sentLst.add(sent);

            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return sentLst;

    }

    public static Set<String> getRandomPerLst() throws SQLException {

        Connection dbConnection = null;
        Statement statement = null;
        List<String> PerLst = new LinkedList();
        String selectTableSQL = "select perEntity from sentenceLst where perEntity IS NOT NULL order by RAND()";

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            System.out.println(selectTableSQL);

            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                String per = rs.getString("perEntity");
                if (!per.equals(" ")) {
                    PerLst.add(per);
                }

            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return new HashSet<String>(PerLst);

    }

    public static Set<String> getRandomLocLst() throws SQLException {

        Connection dbConnection = null;
        Statement statement = null;
        //Set<String> LocLst = new HashSet<>();
        List<String> LocLst = new LinkedList();

        String selectTableSQL = "select locEntity from sentenceLst where locEntity IS NOT NULL order by RAND()";

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            System.out.println(selectTableSQL);

            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                String loc = rs.getString("locEntity");
                if (!loc.equals(" ")) {
                    LocLst.add(loc);
                }

            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return new HashSet<String>(LocLst);

    }

    public static Set<String> getRandomOrgLst() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        List<String> OrgLst = new LinkedList();

        String selectTableSQL = "select orgEntity from sentenceLst where orgEntity IS NOT NULL order by RAND()";

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            System.out.println(selectTableSQL);

            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                String org = rs.getString("orgEntity");
                if (!org.equals(" ")) {
                    OrgLst.add(org);
                }
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return new HashSet<String>(OrgLst);

    }

    public static String getQuesHeader(int quesNo) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String quesHeader = null;
        String selectTableSQL = "SELECT QuesHeadTitle FROM QuesTypes "
                + "where id =" + quesNo;

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            System.out.println(selectTableSQL);

            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                quesHeader = rs.getString("QuesHeadTitle");
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return quesHeader;

    }

    public static Set<String> getQuesLst(int quesType, int quesNo) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        Set<String> quesLst = new HashSet<>();
        String selectTableSQL = "SELECT quesTxt FROM quesLst where QuesTypeID = "
                + quesType + " order by rand() limit " + quesNo;

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                quesLst.add(rs.getString("quesTxt"));
                //System.out.println("teeeeeet");
            }

            System.out.println(selectTableSQL);
            System.out.println("quesLst loaded");

        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return quesLst;

    }

    public static Set<String> getQuesLst(int quesType) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        Set<String> quesLst = new HashSet<>();
        String selectTableSQL = "SELECT quesTxt FROM quesLst where QuesTypeID = "
                + quesType + " order by rand() ";

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                quesLst.add(rs.getString("quesTxt"));
                //System.out.println("teeeeeet");
            }

            System.out.println(selectTableSQL);
            System.out.println("quesLst loaded");

        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return quesLst;

    }

    public static void truncateTables() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;

        String truncateTableSQL1 = "TRUNCATE FullTxt";
        String truncateTableSQL2 = "TRUNCATE sentenceLst";
        String truncateTableSQL3 = "TRUNCATE quesLst";

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            dbConnection.setAutoCommit(false);

            statement.addBatch(truncateTableSQL1);
            statement.addBatch(truncateTableSQL2);
            statement.addBatch(truncateTableSQL3);

            statement.executeBatch();

            dbConnection.commit();

            System.out.println("tables truncated successfuly");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }

    }

    private static Connection getDBConnection() {

        Connection dbConnection = null;

        try {

            Class.forName(DB_DRIVER);

        } catch (ClassNotFoundException e) {

            System.out.println(e.getMessage());

        }

        try {

            dbConnection = (Connection) DriverManager.getConnection(
                    DB_CONNECTION + DB_UNICODE, DB_USER, DB_PASSWORD);
            return dbConnection;

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        return dbConnection;

    }

}
