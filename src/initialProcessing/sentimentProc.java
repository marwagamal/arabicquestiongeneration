package initialProcessing;

import ARScriptProcessing.ANER;
import ARScriptProcessing.Arparsing;
import edu.stanford.nlp.international.arabic.process.ArabicSegmenter;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.analysis.ar.ArabicNormalizer;

/**
 *
 * @author Marwa Gamal <http://marwagamal.com/>
 */
public class sentimentProc {

    ArabicSegmenter segm;
    ArabicNormalizer normalizer;
    Arparsing parser;
    ANER namedEntity;

    public sentimentProc() {

        // set properties for segmenter
        Properties props = new Properties();
        props.put("serDictionary", "data/arabic-segmenter-atbtrain.ser.gz");
        props.put("inputEncoding", "UTF-8");
        //props.put("orthoOptions", "normAlif,normYa,removeDiacritics,removeTatweel,removeQuranChars,atbEscaping");
        // load segmenter
        segm = new ArabicSegmenter(props);
        segm.loadSegmenter("data/arabic-segmenter-atbtrain.ser.gz", props);

        //load Gazetter
        namedEntity = new ANER();

        //load Parser
        parser = new Arparsing();
    }

    public void exetractSent(String fullText) {

        for (String sent : fullText.split("\\n|[.](?<!\\\\d)(?!\\\\d)")) {
            sent = sent.trim();
            //sent = getArabicNumbers(sent);
            if (!sent.isEmpty()) {
                InsertIntoDB(sent);
            }
        }

    }

   

    public void InsertIntoDB(String sent) {

        try {

            String normalizedSent = normalize(sent);
            String parsedSent = parser.parse(segm(normalizedSent)).pennString();
            String perEntity = namedEntity.extractPerEntities(normalizedSent);
            String locEntity = namedEntity.extractLocEntities(normalizedSent);
            String orgEntity = namedEntity.extractOrgEntities(normalizedSent);

            DBConector.insertSentLst(sent, normalizedSent, segm(normalizedSent),
                    parsedSent, perEntity, locEntity, orgEntity, "");

        } catch (SQLException ex) {
            Logger.getLogger(sentimentProc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String segm(String text) {

        return segm.segmentString(text);

    }

    public String normalize(String sentence) {
        normalizer = new ArabicNormalizer();
        char[] CharArray = sentence.toCharArray();
        int length = normalizer.normalize(CharArray, CharArray.length);
        return String.valueOf(CharArray, 0, length);

    }

}
