/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package initialProcessing;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 *
 * @author Marwa Gamal <http://marwagamal.com/>
 */
public class FileProc {

    private File file;
    FileOutputStream fop = null;

    public FileProc(File file) {
        this.file = file;
    }

    public void processFile() {

    }

    public String retrieveScript() {
        String content = "";
        try {
            WordExtractor extractor = null;
            XWPFWordExtractor docxextractor = null;

            if (FilenameUtils.getExtension(file.getAbsolutePath()).equals("doc")) {
                FileInputStream fis = null;

                fis = new FileInputStream(file.getAbsolutePath());
                HWPFDocument document = new HWPFDocument(fis);
                extractor = new WordExtractor(document);
                String[] fileData = extractor.getParagraphText();
                for (int i = 0; i < fileData.length; i++) {
                    if (fileData[i] != null) {
                        content += fileData[i];
                    }
                }

            } else if (FilenameUtils.getExtension(file.getAbsolutePath()).equals("docx")) {
                FileInputStream fis = null;

                fis = new FileInputStream(file.getAbsolutePath());
                XWPFDocument document = new XWPFDocument(fis);
                docxextractor = new XWPFWordExtractor(document);
                String[] fileData = extractor.getParagraphText();
                for (int i = 0; i < fileData.length; i++) {
                    if (fileData[i] != null) {
                        content += fileData[i];
                    }
                }

            } else {

                DataInputStream dis = new DataInputStream(new FileInputStream(file));
                byte[] datainBytes = new byte[dis.available()];
                dis.readFully(datainBytes);
                dis.close();
                content = new String(datainBytes, 0, datainBytes.length);

            }
            DBConector.insertIntoScriptDBTable(content);

        } catch (SQLException ex) {
            content = "حدث خطأ أثناء عملية اضافة النصوص الى قواعد البيانات";
            ex.printStackTrace();
        } catch (Exception ex) {
            content = "حدث خطأ أثناء عملية استرجاع النص من الملف المرفق";
            ex.printStackTrace();
        }

        return content;

    }

    public void saveWordDoc(String path, String content) {

        file = new File(path);
        FileOutputStream outStream = null;

        XWPFDocument document = new XWPFDocument();
        XWPFParagraph paragraphOne = document.createParagraph();
        XWPFRun paragraphOneRunOne = paragraphOne.createRun();
        paragraphOneRunOne.setText(content);

        try {

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            outStream = new FileOutputStream(path);

            document.write(outStream);
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveFile(String path, String content) {

        try {

            file = new File(path);
            fop = new FileOutputStream(file);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public boolean validateFileType() {

        String fileExt = file.getName().substring(file.getName().indexOf(".") + 1);
        if (fileExt.equals("doc") || fileExt.equals("txt")) {
            //MsgLbl = "الملف يجب أن يكون ملف نصى ويحتوى على النصوص العربية";
            return true;
        } else {
            return false;
        }

    }

    public boolean ValidateFile() {
        if (!file.isFile()) {
            //MsgLbl = "الملف غير سليم";
            return false;
        } else {
            return true;
        }
    }

    public boolean validateFileSize() {
        if (file.length() > 1048576) {
            //MsgLbl = "حجم الملف أكبر من اللازم";
            return false;
        } else {
            return true;
        }
    }

    public boolean validateArabicText(String s) {
        for (int i = 0; i < Character.codePointCount(s, 0, s.length()); i++) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06FF) {
                return true;
            }
        }
        return false;

    }

}
