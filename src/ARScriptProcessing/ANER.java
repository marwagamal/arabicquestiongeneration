/*
 * Copyright 2014 Marwa Gamal <http://marwagamal.com/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ARScriptProcessing;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Marwa Gamal <http://marwagamal.com/>
 */
public class ANER {

    DataInputStream dis = null;

    public ANER() {

    }

    private Set<String> loadGazetteer(String filename) {
        // load  Gazetteer
        List<String> lineLst = new LinkedList();
        try {

            dis = new DataInputStream(new FileInputStream(new File(filename)));
            BufferedReader br = new BufferedReader(new InputStreamReader(dis));
            String strLine;
            //Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                lineLst.add(strLine.trim());
            }
            br.close();
            dis.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }
        return new HashSet<String>(lineLst);

    }

    public String extractPerEntities(String sent) {
        // call Per Gazetteer 
        Set<String> nameLst = loadGazetteer("data/AlignedPersGazetteer");
        String name = "";

        for (String token : sent.split("\\s+")) {
            for (String per : nameLst) {
                if (token.equals(per)) {
                    name = name + " " + token;

                    break;
                }
            }
            if (wordcount(name) >= 2) {
                break;
            }

        }

        return name;

    }

    private int wordcount(String line) {
        int wordChar = 0;
        boolean prevCharWasSpace = true;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == ' ') {
                prevCharWasSpace = true;
            } else {
                if (prevCharWasSpace) {
                    wordChar++;
                }
                prevCharWasSpace = false;

            }
        }
        return wordChar;
    }

    public String extractOrgEntities(String sent) {
        // call Org Gazetteer 
        Set<String> orgLst = loadGazetteer("data/AlignedOrgGazetteer");

        String org = "";
        for (String per : orgLst) {

            if (sent.contains(" " + per + " ")) {
                org += per;
                break;
            }

        }

        return org;

    }

    public String extractLocEntities(String sent) {
        // call Loc Gazetteer 
        Set<String> locLst = loadGazetteer("data/AlignedLocGazetteer");

        String loc = "";
        for (String per : locLst) {
            if (sent.contains(" " + per + " ")) {
                loc += per;
                break;
            }

        }
        return loc;

    }

}
