/*
 * Copyright 2014 Marwa Gamal <http://marwagamal.com/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package QuesExtraction;

import initialProcessing.DBConector;

/**
 *
 * @author Marwa Gamal <http://marwagamal.com/>
 */
public class generateExam {

    public String getArabicNumbers(String str) {

        char[] arabicChars = {'٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'};
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                builder.append(arabicChars[(int) (str.charAt(i)) - 48]);
            } else {
                builder.append(str.charAt(i));
            }
        }

        return builder.toString();

    }

    public StringBuilder loadExam() {
        StringBuilder exam = new StringBuilder();

        try {

            // set exam header info 
            exam.append("\n\n\n\n");

            //insert horizon break 
            //first ques
            exam.append("السؤال الأول:  ");
            exam.append(DBConector.getQuesHeader(3));
            exam.append(System.getProperty("line.separator"));
            int num1 = 1;
            for (String s : DBConector.getQuesLst(3)) {

                exam.append(getArabicNumbers(String.valueOf(num1)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num1++;
            }

            exam.append(System.getProperty("line.separator"));
            exam.append(System.getProperty("line.separator"));

            //second ques
            exam.append("السؤال الثانى:  ");
            exam.append(DBConector.getQuesHeader(2));
            exam.append(System.getProperty("line.separator"));
            int num2 = 1;
            for (String s : DBConector.getQuesLst(2, 7)) {

                exam.append(getArabicNumbers(String.valueOf(num2)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num2++;
            }

            exam.append(System.getProperty("line.separator"));
            exam.append(System.getProperty("line.separator"));

            //third ques
            exam.append("السؤال الثالث:  ");
            exam.append(DBConector.getQuesHeader(1));
            exam.append(System.getProperty("line.separator"));
            int num3 = 1;
            for (String s : DBConector.getQuesLst(1)) {

                exam.append(getArabicNumbers(String.valueOf(num3)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num3++;
            }

            exam.append(System.getProperty("line.separator"));
            exam.append(System.getProperty("line.separator"));

            //forth ques
            exam.append("السؤال الرابع:  ");
            exam.append(DBConector.getQuesHeader(4));
            exam.append(System.getProperty("line.separator"));
            int num4 = 1;
            for (String s : DBConector.getQuesLst(4, 10)) {

                exam.append(getArabicNumbers(String.valueOf(num4)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num4++;
            }
        } catch (Exception ex) {
        }
        return exam;

    }

    public StringBuilder loadFullQues() {
        StringBuilder exam = new StringBuilder();
        try {

            // set exam header info 
            exam.append("\n\n\n\n");

            //insert horizon break 
            //first ques
            exam.append("السؤال الأول:  ");
            exam.append(DBConector.getQuesHeader(3));
            exam.append(System.getProperty("line.separator"));
            int num1 = 1;
            for (String s : DBConector.getQuesLst(3)) {

                exam.append(getArabicNumbers(String.valueOf(num1)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num1++;
            }

            exam.append(System.getProperty("line.separator"));
            exam.append(System.getProperty("line.separator"));

            //second ques
            exam.append("السؤال الثانى:  ");
            exam.append(DBConector.getQuesHeader(2));
            exam.append(System.getProperty("line.separator"));
            int num2 = 1;
            for (String s : DBConector.getQuesLst(2)) {

                exam.append(getArabicNumbers(String.valueOf(num2)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num2++;
            }

            exam.append(System.getProperty("line.separator"));
            exam.append(System.getProperty("line.separator"));

            //third ques
            exam.append("السؤال الثالث:  ");
            exam.append(DBConector.getQuesHeader(1));
            exam.append(System.getProperty("line.separator"));
            int num3 = 1;
            for (String s : DBConector.getQuesLst(1)) {

                exam.append(getArabicNumbers(String.valueOf(num3)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num3++;
            }

            exam.append(System.getProperty("line.separator"));
            exam.append(System.getProperty("line.separator"));

            //forth ques
            exam.append("السؤال الرابع:  ");
            exam.append(DBConector.getQuesHeader(4));
            exam.append(System.getProperty("line.separator"));
            int num4 = 1;
            for (String s : DBConector.getQuesLst(4)) {

                exam.append(getArabicNumbers(String.valueOf(num4)) + ". ");
                exam.append(getArabicNumbers(s));
                exam.append(System.getProperty("line.separator"));
                num4++;
            }
        } catch (Exception ex) {
        }
        return exam;

    }

//    public String testLoadEx() {
//
//        try {
//            return DBConector.getQuesLst(4, 1);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
