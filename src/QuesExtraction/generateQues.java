/*
 * Copyright 2014 Marwa Gamal <http://marwagamal.com/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package QuesExtraction;

import edu.stanford.nlp.trees.HeadFinder;
import edu.stanford.nlp.trees.PennTreeReader;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.international.arabic.ArabicHeadFinder;
import edu.stanford.nlp.trees.international.arabic.ArabicTreebankLanguagePack;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.trees.tregex.TregexPatternCompiler;
import edu.stanford.nlp.util.Function;
import initialProcessing.DBConector;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Marwa Gamal <http://marwagamal.com/>
 */
public class generateQues {

    TregexPatternCompiler patternCompiler;

    /*
     ques 1: multiChoise
     ques 2: trueFalse
     ques 3: complete 
     ques 4: answer
     ques #: explain
     ques #: define
     ques #: compare
     ques #: wh-
    
     */
    public void generateQuesLst() {
        try {
            List<String> sentLst = DBConector.selectSentLst();
            for (String sent : sentLst) {
                //System.out.println(sent);
                String[] sentTokens = sent.split("\\*\\*\\*");
                generate(sentTokens[0], sentTokens[1], sentTokens[2],
                        sentTokens[3],
                        sentTokens[4]);

            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }

    public void generate(String normalizedSent, String parseSent, String perEntity,
            String locEntity, String orgEntity) {

        try {

            Object[] dist;

            matchCompleteQues(parseSent, normalizedSent);
            matchDefineQes(normalizedSent);
            if (contatinsInteger(normalizedSent)) {
                generateTimeQues(normalizedSent);
            }
            if (!perEntity.isEmpty() && perEntity.length() > 2 && !perEntity.equals(" ") && !perEntity.equals("-")) {
                dist = DBConector.getRandomPerLst().toArray();
                generatePerQues(normalizedSent, perEntity,
                        dist[2].toString(), dist[1].toString());

            }
            if (!locEntity.isEmpty() && locEntity.length() > 2 && !locEntity.equals(" ") && !locEntity.equals("-")) {
                dist = DBConector.getRandomLocLst().toArray();
                generateLocQues(normalizedSent, locEntity, dist[1].toString(),
                        dist[2].toString());

            }
            if (!orgEntity.isEmpty() && orgEntity.length() > 2 && !orgEntity.equals(" ") && !orgEntity.equals("-")) {
                dist = DBConector.getRandomOrgLst().toArray();
                generateOrgQues(normalizedSent, orgEntity, dist[2].toString(),
                        dist[1].toString());

            }

        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }

    private void matchDefineQes(String sent) {
        try {
            // extract time/date 
            Pattern p = Pattern.compile("\\s+[\u0647,\u0648,\u0645,\u0649]{2}\\s+");
            Matcher m = p.matcher(sent);
            if (m.find()) {
                // insert complete ques
                DBConector.insertQues(" ...... " + sent.substring(sent.indexOf(m.group())), 3, "");

                // insert essay ques
                DBConector.insertQues("عرف " + sent.substring(0, sent.indexOf(m.group())) + " ؟", 4, "");
                // insert essay ques
                DBConector.insertQues("من " + sent.substring(sent.indexOf(m.group())) + " ؟", 4, "");

            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    private void matchCompleteQues(String sentTree, String sentStr) {
        try {
            PennTreeReader reader = new PennTreeReader(new StringReader(sentTree));
            Tree sent = reader.readTree();

            // tregex loading
            HeadFinder hf = new ArabicHeadFinder();
            TreebankLanguagePack arabicTlp = new ArabicTreebankLanguagePack();
            Function bcf = arabicTlp.getBasicCategoryFunction();
            patternCompiler = new TregexPatternCompiler(hf, bcf);

//            // ques 2: complete 
//            TregexPattern p = patternCompiler.compile(" PP <, IN & $. NP & >> NP|VP");
//            TregexMatcher m = p.matcher(sent);
//            //System.out.println("start patterns");
//            while (m.find()) {
//
//                // System.out.println("start matching");
//                String match = m.getMatch().label().toString();
//                //System.out.println(match);
//                String ques = sentStr.substring(0,
//                        sentStr.indexOf(match.substring(match.indexOf("=") + 1,
//                                        match.indexOf("PartOfSpeechAnnotation")))) + "....";
//                DBConector.insertQues(ques, 3, "");
//            }
            // ques 2: true-flase 
            TregexPattern p2 = patternCompiler.compile("NP|VP < (DTNN|DTNNS $. DTJJ) & ,, NP|NN|NNS|NNPS|NNP|DTNN|DTNNS");
            TregexMatcher m2 = p2.matcher(sent);
            //System.out.println("start patterns");
            while (m2.find()) {

                DBConector.insertQues(sentStr, 2, "");
            }

        } catch (Exception ex) {
            Logger.getLogger(generateQues.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private boolean contatinsInteger(String line) {
        try {

            Pattern intsOnly = Pattern.compile("\\d{4}");
            Matcher makeMatch = intsOnly.matcher(line);

            if (makeMatch.find()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }

    }

    private void generateTimeQues(String sent) {
        int digits = 0;
        try {

            // extract time/date 
            Pattern p = Pattern.compile("\\d{4}");
            Matcher m = p.matcher(sent);
            if (m.find()) {
                digits = Integer.parseInt(m.group());
            }

            // insert complete ques
            DBConector.insertQues(sent.replaceAll("\\s+\\d{4}", " ...... "), 3, "");
            // insert true-false ques
            DBConector.insertQues(sent.replaceAll("\\d{4}",
                    String.valueOf(new Random().nextInt(2000))), 2, "");
            // insert multichoise ques
            DBConector.insertQues(sent.replaceFirst("[\u0600-\u06FF]+\\s+\\d{4}.*", "  ......  ") + " \n ("
                    + String.valueOf(new Random().nextInt(2000)) + " - "
                    + String.valueOf(new Random().nextInt(2000)) + " - "
                    + digits + " - "
                    + String.valueOf(new Random().nextInt(2000))
                    + ")", 1, "");

            // insert essay ques
            DBConector.insertQues("متى " + sent.replaceAll("[\u0600-\u06FF]+\\s+\\d+.*", " ") + " ؟", 4, "");
        } catch (SQLException ex) {
            Logger.getLogger(generateQues.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void generatePerQues(String sent, String per, String dist1, String dist2) {

        try {

            DBConector.insertQues(sent.replaceAll(per, " ...... "), 3, "");

            // insert true-false ques
            DBConector.insertQues(sent.replaceAll(per, dist1), 2, "");
            // insert multichoise ques

            DBConector.insertQues(sent.replaceAll(per, " ...... ") + " \n ("
                    + dist1 + " - " + dist2 + " - " + per + ")", 1,
                    "");

            // insert essay ques
            DBConector.insertQues("من " + sent.replaceAll(per, " ") + " ؟", 4, "");
        } catch (SQLException ex) {
            Logger.getLogger(generateQues.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void generateLocQues(String sent, String loc, String dist1, String dist3) {

        try {
            // insert complete ques
            DBConector.insertQues(sent.replaceFirst(loc, " ...... "), 3, "");
            // insert true-false ques
            DBConector.insertQues(sent.replaceFirst(loc, dist1), 2, "");
            // insert multichoise ques
            DBConector.insertQues(sent.replaceFirst(loc, " ...... ") + " \n ("
                    + dist1 + " - "
                    + loc + " - "
                    + dist3 + ")", 1, "");
            // insert essay ques
            DBConector.insertQues("أين " + sent.replace(loc, " ") + " ؟", 4, "");
        } catch (SQLException ex) {
            Logger.getLogger(generateQues.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void generateOrgQues(String sent, String org, String dist1, String dist3) {

        try {
            // insert complete ques
            DBConector.insertQues(sent.replaceAll(org, " ...... "), 3, "");
            // insert true-false ques
            DBConector.insertQues(sent.replaceAll(org, dist1), 2, "");
            // insert multichoise ques
            DBConector.insertQues(sent.replaceAll(org, " ...... ") + " \n ("
                    + org + " - "
                    + dist1 + " - "
                    + dist3 + ")", 1, "");

            // insert essay ques
            //DBConector.insertQues("لماذا " + sent.substring(0, sent.lastIndexOf(org)) + " ؟", 4, "");

        } catch (SQLException ex) {
            Logger.getLogger(generateQues.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
